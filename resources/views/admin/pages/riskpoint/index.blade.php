@extends('admin.layouts.main')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div style="text-align: center;font-size: 30px;font-weight: bolder;color: #0b2e13" class="card-header">Riskpoint</div>
                    @if (\Session::has('success'))
                        <div class="alert alert-success">
                            <p>{{ \Session::get('success') }}</p>
                        </div>
                    @endif
                    <div class="card-body">
                        <a href="/riskpoint/create" class="btn btn-primary">Add New</a>
{{--                        <button>Add New</button>--}}
                    </div>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>GroupiD</th>
                        <th>Name</th>
                        <th>View</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                @foreach($riskpoints as $riskpoint)
                <tbody>
                    <tr>
                        <td>{{ $riskpoint['id'] }}</td>
                        <td>{{ $riskpoint['group_riskpoint'] }}</td>
                        <td>{{ $riskpoint['name_riskpoint'] }}</td>
                        <td><a href="{{action('RiskpointController@show',$riskpoint['id'])}}" class="btn btn-primary">View</a></td>
                        <td><a href="{{action('RiskpointController@edit',$riskpoint['id'])}}" class="btn btn-primary">Edit</a></td>

                        <td>
                            <form method="post" class="delete_form" action="{{action('RiskpointController@destroy',$riskpoint['id'])}}">
                                {{csrf_field()}}
                                <input type="hidden" name="_method" value="DELETE"/>
                                <button type="submit" class="btn btn-danger"  onclick="return confirm('คุณต้องการลบข้อมูลหรือไม่ ?')">Delete</button>
                            </form>
                        </td>
                    </tr>
                </tbody>
                @endforeach
            </table>
        </div>
    </div>
{{--    <script type="text/javascript">--}}
{{--        $(document).ready(function(){--}}
{{--            $('.delete_form').on('submit',function () {--}}
{{--                if (confirm("คุณต้องการลบข้อมูลหรือไม่ ?")){--}}
{{--                    return true;--}}
{{--                }--}}
{{--                else {--}}
{{--                    return false;--}}
{{--                }--}}
{{--            });--}}
{{--        });--}}
{{--    </script>--}}
@endsection
