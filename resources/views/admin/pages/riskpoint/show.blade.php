@extends('admin.layouts.main')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div style="text-align: center;font-size: 30px;font-weight: bolder;color: #0b2e13" class="card-header"> Show Product</div>
                    <div class="card-body">
                        <a href="/riskpoint" class="btn btn-primary">ย้อนกลับ</a>
                    </div>
                </div>
            </div>
        </div>
        <form method="post" action="{{url('riskpoint')}}">
            {{csrf_field()}}
            <div style="margin-bottom: 3%;margin-top: 3%">
                <div class="form-group row">
                    <h3 class="column" style="width: 15%;text-align: right">Groud ID: </h3>
                    <h4 class="column" style="margin-top: 4px;margin-left: 3%">{{$riskpoint['group_riskpoint']}}</h4>
                </div>
                <div class="form-group row">
                    <h3 class="column" style="width: 15%;text-align: right">Name:</h3>
                    <h4 class="column" style="margin-top: 4px;margin-left: 3%">{{$riskpoint['name_riskpoint']}}</h4>
                </div>
            </div>
        </form>
    </div>
@endsection
