@extends('admin.layouts.main')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div style="text-align: center;font-size: 30px;font-weight: bolder;color: #0b2e13" class="card-header">Edit Riskpoint</div>
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul> @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    @if (\Session::has('success'))
                        <div class="alert alert-success">
                            <p>{{ \Session::get('success') }}</p>
                        </div>
                    @endif
                    <div class="card-body">
                        <a href="/riskpoint" class="btn btn-primary">ย้อนกลับ</a>
                    </div>
                </div>
            </div>
        </div>
        <form method="POST" action="{{ route('riskpoint.update',['riskpoint'=>$id]) }}">
            {{csrf_field()}}
            <input name="_method" type="hidden" value="PUT">
            <div style="margin-bottom: 3%;margin-top: 3%">
                <div class="form-group row">
                    <h3 class="column" style="width: 15%;text-align: right">Groud ID: </h3>
                    <input class="column" style="margin-left: 1%;padding-left: 1%;padding-bottom: 4px" type="text" name="group_riskpoint" class="form-control" placeholder="GroudID" value="{{$riskpoint['group_riskpoint']}}" />
                </div>
                <div class="form-group row">
                    <h3 class="column" style="width: 15%;text-align: right">Name:</h3>
                    <input class="column" style="margin-left: 1%;padding-left: 1%;padding-bottom: 4px" type="text" name="name_riskpoint" class="form-control" placeholder="Name" value="{{$riskpoint['name_riskpoint']}}" />
                </div>
                <div class="form-group" style="text-align: center">
                    {{--                <input type="hidden" name="_method" value="PATCH"/>--}}
                    <input type="submit" class="btn btn-primary" value="อัพเดต">
                </div>
            </div>

        </form>
    </div>
@endsection
