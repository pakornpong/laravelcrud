@extends('admin.layouts.main')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div style="text-align: center;font-size: 30px;font-weight: bolder;color: #0b2e13" class="card-header">Create New Patient</div>
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul> @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    @if (\Session::has('success'))
                        <div class="alert alert-success">
                            <p>{{ \Session::get('success') }}</p>
                        </div>
                    @endif
                    <div class="card-body">
                        <a href="/patient" class="btn btn-primary">ย้อนกลับ</a>
                    </div>
                </div>
            </div>
        </div>
        <form method="post" action="{{url('patient')}}">
            {{csrf_field()}}
            <div class="form-group row">
                <h3 class="column" style="width: 20%;text-align: right">dead_conso: </h3>
{{--                <div class="row">--}}
{{--                    <strong>GropID:</strong>--}}
                    <input class="column" type="text" name="dead_conso" style="margin-left: 1%;padding-left: 1%" class="form-control"  placeholder="dead_conso" />
{{--                </div>--}}
            </div>
            <div class="form-group row">
                <h3 class="column" style="width: 20%;text-align: right">dead_year: </h3>
                <input class="column" type="text" name="dead_year" style="margin-left: 1%;padding-left: 1%" class="form-control"  placeholder="dead_year" />
            </div>
            <div class="form-group row">
                <h3 class="column" style="width: 20%;text-align: right">acc_no: </h3>
                <input class="column" type="text" name="acc_no" style="margin-left: 1%;padding-left: 1%" class="form-control"  placeholder="acc_no" />
            </div>
            <div class="form-group row">
                <h3 class="column" style="width: 20%;text-align: right">fname: </h3>
                <input class="column" type="text" name="fname" style="margin-left: 1%;padding-left: 1%" class="form-control"  placeholder="fname" />
            </div>
            <div class="form-group row">
                <h3 class="column" style="width: 20%;text-align: right">lname: </h3>
                <input class="column" type="text" name="lname" style="margin-left: 1%;padding-left: 1%" class="form-control"  placeholder="lname" />
            </div>
            <div class="form-group row">
                <h3 class="column" style="width: 20%;text-align: right">prefix: </h3>
                <input class="column" type="text" name="prefix" style="margin-left: 1%;padding-left: 1%" class="form-control"  placeholder="prefix" />
            </div>
            <div class="form-group row">
                <h3 class="column" style="width: 20%;text-align: right">drv_socno: </h3>
                <input class="column" type="text" name="drv_socno" style="margin-left: 1%;padding-left: 1%" class="form-control"  placeholder="drv_socno" />
            </div>
            <div class="form-group row">
                <h3 class="column" style="width: 20%;text-align: right">age: </h3>
                <input class="column" type="text" name="age" style="margin-left: 1%;padding-left: 1%" class="form-control"  placeholder="age" />
            </div>
            <div class="form-group row">
                <h3 class="column" style="width: 20%;text-align: right">sex: </h3>
                <input class="column" type="text" name="sex" style="margin-left: 1%;padding-left: 1%" class="form-control"  placeholder="sex" />
            </div>
            <div class="form-group row">
                <h3 class="column" style="width: 20%;text-align: right">birth_date: </h3>
                <input class="column" type="text" name="birth_date" style="margin-left: 1%;padding-left: 1%" class="form-control"  placeholder="birth_date" />
            </div>
            <div class="form-group row">
                <h3 class="column" style="width: 20%;text-align: right">career_id: </h3>
                <input class="column" type="text" name="career_id" style="margin-left: 1%;padding-left: 1%" class="form-control"  placeholder="career_id" />
            </div>
            <div class="form-group row">
                <h3 class="column" style="width: 20%;text-align: right">nationality: </h3>
                <input class="column" type="text" name="nationality" style="margin-left: 1%;padding-left: 1%" class="form-control"  placeholder="nationality" />
            </div>
            <div class="form-group row">
                <h3 class="column" style="width: 20%;text-align: right">subdistrict: </h3>
                <input class="column" type="text" name="subdistrict" style="margin-left: 1%;padding-left: 1%" class="form-control"  placeholder="subdistrict" />
            </div>
            <div class="form-group row">
                <h3 class="column" style="width: 20%;text-align: right">district: </h3>
                <input class="column" type="text" name="district" style="margin-left: 1%;padding-left: 1%" class="form-control"  placeholder="district" />
            </div>
            <div class="form-group row">
                <h3 class="column" style="width: 20%;text-align: right">province: </h3>
                <input class="column" type="text" name="province" style="margin-left: 1%;padding-left: 1%" class="form-control"  placeholder="province" />
            </div>
{{--            <div class="form-group row">
class="column" --}}
{{--                <input type="text" name="risk_algohol" style="margin-left: 1%;padding-left: 1%" class="form-control"  placeholder="risk_algohol" />--}}
{{--            </div>--}}
{{--            <div class="form-group row">
class="column" --}}
{{--                <input type="text" name="risk_helmet" style="margin-left: 1%;padding-left: 1%" class="form-control"  placeholder="risk_helmet" />--}}
{{--            </div>--}}
{{--            <div class="form-group row">
class="column" --}}
{{--                <input type="text" name="risk_safety" style="margin-left: 1%;padding-left: 1%" class="form-control"  placeholder="risk_safety" />--}}
{{--            </div>--}}
            <div class="form-group row">
                <h3 class="column" style="width: 20%;text-align: right">dead_date: </h3>
                <input class="column" type="text" name="dead_date" style="margin-left: 1%;padding-left: 1%" class="form-control"  placeholder="dead_date" />
            </div>
            <div class="form-group row">
                <h3 class="column" style="width: 20%;text-align: right">victim_no: </h3>
                <input class="column" type="text" name="victim_no" style="margin-left: 1%;padding-left: 1%" class="form-control"  placeholder="victim_no" />
            </div>
            <div class="form-group row">
                <h3 class="column" style="width: 20%;text-align: right">car_license: </h3>
                <input class="column" type="text" name="car_license" style="margin-left: 1%;padding-left: 1%" class="form-control"  placeholder="car_license" />
            </div>
            <div class="form-group row">
                <h3 class="column" style="width: 20%;text-align: right">car_province: </h3>
                <input class="column" type="text" name="car_province" style="margin-left: 1%;padding-left: 1%" class="form-control"  placeholder="car_province" />
            </div>
{{--            <div class="form-group row">
class="column" --}}
{{--                <input type="text" name="type_motor" style="margin-left: 1%;padding-left: 1%" class="form-control"  placeholder="type_motor" />--}}
{{--            </div>--}}
            <div class="form-group row">
                <h3 class="column" style="width: 20%;text-align: right">car_band: </h3>
                <input class="column" type="text" name="car_band" style="margin-left: 1%;padding-left: 1%" class="form-control"  placeholder="car_band" />
            </div>
            <div class="form-group row">
                <h3 class="column" style="width: 20%;text-align: right">drive_name: </h3>
                <input class="column" type="text" name="drive_name" style="margin-left: 1%;padding-left: 1%" class="form-control"  placeholder="drive_name" />
            </div>
{{--            <div class="form-group row">
class="column" --}}
{{--                <input type="text" name="drive_address" style="margin-left: 1%;padding-left: 1%" class="form-control"  placeholder="drive_address" />--}}
{{--            </div>--}}
            <div class="form-group row">
                <h3 class="column" style="width: 20%;text-align: right">drive_province: </h3>
                <input class="column" type="text" name="drive_province" style="margin-left: 1%;padding-left: 1%" class="form-control"  placeholder="drive_province" />
            </div>
            <div class="form-group row">
                <h3 class="column" style="width: 20%;text-align: right">tp_no: </h3>
                <input class="column" type="text" name="tp_no" style="margin-left: 1%;padding-left: 1%" class="form-control"  placeholder="tp_no" />
            </div>
            <div class="form-group row">
                <h3 class="column" style="width: 20%;text-align: right">date_rec: </h3>
                <input class="column" type="text" name="date_rec" style="margin-left: 1%;padding-left: 1%" class="form-control"  placeholder="date_rec" />
            </div>
            <div class="form-group row">
                <h3 class="column" style="width: 20%;text-align: right">time_rec: </h3>
                <input class="column" type="text" name="time_rec" style="margin-left: 1%;padding-left: 1%" class="form-control"  placeholder="time_rec" />
            </div>
            <div class="form-group row">
                <h3 class="column" style="width: 20%;text-align: right">acc_subdist: </h3>
                <input class="column" type="text" name="acc_subdist" style="margin-left: 1%;padding-left: 1%" class="form-control"  placeholder="acc_subdist" />
            </div>
            <div class="form-group row">
                <h3 class="column" style="width: 20%;text-align: right">acc_dist: </h3>
                <input class="column" type="text" name="acc_dist" style="margin-left: 1%;padding-left: 1%" class="form-control"  placeholder="acc_dist" />
            </div>
            <div class="form-group row">
                <h3 class="column" style="width: 20%;text-align: right">acc_prov: </h3>
                <input class="column" type="text" name="acc_prov" style="margin-left: 1%;padding-left: 1%" class="form-control"  placeholder="acc_prov" />
            </div>
            <div class="form-group row">
                <h3 class="column" style="width: 20%;text-align: right">acc_lat: </h3>
                <input class="column" type="text" name="acc_lat" style="margin-left: 1%;padding-left: 1%" class="form-control"  placeholder="acc_lat" />
            </div>
            <div class="form-group row">
                <h3 class="column" style="width: 20%;text-align: right">acc_long: </h3>
                <input class="column" type="text" name="acc_long" style="margin-left: 1%;padding-left: 1%" class="form-control"  placeholder="acc_long" />
            </div>
            <div class="form-group row">
                <h3 class="column" style="width: 20%;text-align: right">is_death: </h3>
                <input class="column" type="text" name="is_death" style="margin-left: 1%;padding-left: 1%" class="form-control"  placeholder="is_death" />
            </div>
            <div class="form-group row">
                <h3 class="column" style="width: 20%;text-align: right">is_e_claim: </h3>
                <input class="column" type="text" name="is_e_claim" style="margin-left: 1%;padding-left: 1%" class="form-control"  placeholder="is_e_claim" />
            </div>
            <div class="form-group row">
                <h3 class="column" style="width: 20%;text-align: right">is_polis: </h3>
                <input class="column" type="text" name="is_polis" style="margin-left: 1%;padding-left: 1%" class="form-control"  placeholder="is_polis" />
            </div>
            <div class="form-group row">
                <h3 class="column" style="width: 20%;text-align: right">protocol: </h3>
                <input class="column" type="text" name="protocol" style="margin-left: 1%;padding-left: 1%" class="form-control"  placeholder="protocol" />
            </div>
            <div class="form-group row">
                <h3 class="column" style="width: 20%;text-align: right">n_cause: </h3>
                <input class="column" type="text" name="n_cause" style="margin-left: 1%;padding-left: 1%" class="form-control"  placeholder="n_cause" />
            </div>
            <div class="form-group row">
                <h3 class="column" style="width: 20%;text-align: right">group_riskpoint: </h3>
                <input class="column" type="text" name="group_riskpoint" style="margin-left: 1%;padding-left: 1%" class="form-control"  placeholder="group_riskpoint" />
            </div>
            <div class="form-group row">
                <h3 class="column" style="width: 20%;text-align: right">group_risk: </h3>
                <input class="column" type="text" name="group_risk" style="margin-left: 1%;padding-left: 1%" class="form-control"  placeholder="group_risk" />
            </div>
            <div class="form-group" style="text-align: center">
                <input type="submit" class="btn btn-primary" value="บันทึก">
            </div>
        </form>
    </div>
@endsection
