@extends('admin.layouts.main')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div style="text-align: center;font-size: 30px;font-weight: bolder;color: #0b2e13" class="card-header"> Show Product</div>
                    <div class="card-body">
                        <a class="btn btn-primary" href="/patient"> Back</a>
                    </div>
                </div>
            </div>
        </div>
        <form method="post" action="{{url('riskpoint')}}">
            {{csrf_field()}}
            <div style="margin-bottom: 3%;margin-top: 3%">
                <div class="form-group row">
                    <h3 class="column" style="width: 15%;text-align: right">dead_conso :</h3>
                    <h4 class="column" style="margin-top: 4px;margin-left: 3%">{{$patient['dead_conso']}}</h4>
                </div>
                <div class="form-group row">
                    <h3 class="column" style="width: 15%;text-align: right">dead_year :</h3>
                    <h4 class="column" style="margin-top: 4px;margin-left: 3%">{{$patient['dead_year']}}</h4>
                </div>
                <div class="form-group row">
                    <h3 class="column" style="width: 15%;text-align: right">acc_no :</h3>
                    <h4 class="column" style="margin-top: 4px;margin-left: 3%">{{$patient['acc_no']}}</h4>
                </div>
                <div class="form-group row">
                    <h3 class="column" style="width: 15%;text-align: right">fname :</h3>
                    <h4 class="column" style="margin-top: 4px;margin-left: 3%">{{$patient['fname']}}</h4>
                </div>
                <div class="form-group row">
                    <h3 class="column" style="width: 15%;text-align: right">lname :</h3>
                    <h4 class="column" style="margin-top: 4px;margin-left: 3%">{{$patient['lname']}}</h4>
                </div>
                <div class="form-group row">
                    <h3 class="column" style="width: 15%;text-align: right">prefix :</h3>
                    <h4 class="column" style="margin-top: 4px;margin-left: 3%">{{$patient['prefix']}}</h4>
                </div>
                <div class="form-group row">
                    <h3 class="column" style="width: 15%;text-align: right">drv_socno :</h3>
                    <h4 class="column" style="margin-top: 4px;margin-left: 3%">{{$patient['drv_socno']}}</h4>
                </div>
                <div class="form-group row">
                    <h3 class="column" style="width: 15%;text-align: right">age :</h3>
                    <h4 class="column" style="margin-top: 4px;margin-left: 3%">{{$patient['age']}}</h4>
                </div>
                <div class="form-group row">
                    <h3 class="column" style="width: 15%;text-align: right">sex :</h3>
                    <h4 class="column" style="margin-top: 4px;margin-left: 3%">{{$patient['sex']}}</h4>
                </div>
                <div class="form-group row">
                    <h3 class="column" style="width: 15%;text-align: right">birth_date :</h3>
                    <h4 class="column" style="margin-top: 4px;margin-left: 3%">{{$patient['birth_date']}}</h4>
                </div>
                <div class="form-group row">
                    <h3 class="column" style="width: 15%;text-align: right">career_id :</h3>
                    <h4 class="column" style="margin-top: 4px;margin-left: 3%">{{$patient['career_id']}}</h4>
                </div>
                <div class="form-group row">
                    <h3 class="column" style="width: 15%;text-align: right">nationality :</h3>
                    <h4 class="column" style="margin-top: 4px;margin-left: 3%">{{$patient['nationality']}}</h4>
                </div>
                <div class="form-group row">
                    <h3 class="column" style="width: 15%;text-align: right">subdistrict :</h3>
                    <h4 class="column" style="margin-top: 4px;margin-left: 3%">{{$patient['subdistrict']}}</h4>
                </div>
                <div class="form-group row">
                    <h3 class="column" style="width: 15%;text-align: right">district :</h3>
                    <h4 class="column" style="margin-top: 4px;margin-left: 3%">{{$patient['district']}}</h4>
                </div>
                <div class="form-group row">
                    <h3 class="column" style="width: 15%;text-align: right">province :</h3>
                    <h4 class="column" style="margin-top: 4px;margin-left: 3%">{{$patient['province']}}</h4>
                </div>
                <div class="form-group row">
                    <h3 class="column" style="width: 15%;text-align: right">risk_algohol :</h3>
                    <h4 class="column" style="margin-top: 4px;margin-left: 3%">{{$patient['risk_algohol']}}</h4>
                </div>
                <div class="form-group row">
                    <h3 class="column" style="width: 15%;text-align: right">risk_helmet :</h3>
                    <h4 class="column" style="margin-top: 4px;margin-left: 3%">{{$patient['risk_helmet']}}</h4>
                </div>
                <div class="form-group row">
                    <h3 class="column" style="width: 15%;text-align: right">risk_algohol :</h3>
                    <h4 class="column" style="margin-top: 4px;margin-left: 3%">{{$patient['risk_algohol']}}</h4>
                </div>
                <div class="form-group row">
                    <h3 class="column" style="width: 15%;text-align: right">risk_safety :</h3>
                    <h4 class="column" style="margin-top: 4px;margin-left: 3%">{{$patient['risk_safety']}}</h4>
                </div>
                <div class="form-group row">
                    <h3 class="column" style="width: 15%;text-align: right">dead_date :</h3>
                    <h4 class="column" style="margin-top: 4px;margin-left: 3%">{{$patient['dead_date']}}</h4>
                </div>
                <div class="form-group row">
                    <h3 class="column" style="width: 15%;text-align: right">victim_no :</h3>
                    <h4 class="column" style="margin-top: 4px;margin-left: 3%">{{$patient['victim_no']}}</h4>
                </div>
                <div class="form-group row">
                    <h3 class="column" style="width: 15%;text-align: right">car_license :</h3>
                    <h4 class="column" style="margin-top: 4px;margin-left: 3%">{{$patient['car_license']}}</h4>
                </div>
                <div class="form-group row">
                    <h3 class="column" style="width: 15%;text-align: right">car_province :</h3>
                    <h4 class="column" style="margin-top: 4px;margin-left: 3%">{{$patient['car_province']}}</h4>
                </div>
                <div class="form-group row">
                    <h3 class="column" style="width: 15%;text-align: right">type_motor :</h3>
                    <h4 class="column" style="margin-top: 4px;margin-left: 3%">{{$patient['type_motor']}}</h4>
                </div>
                <div class="form-group row">
                    <h3 class="column" style="width: 15%;text-align: right">car_band :</h3>
                    <h4 class="column" style="margin-top: 4px;margin-left: 3%">{{$patient['car_band']}}</h4>
                </div>
                <div class="form-group row">
                    <h3 class="column" style="width: 15%;text-align: right">drive_name :</h3>
                    <h4 class="column" style="margin-top: 4px;margin-left: 3%">{{$patient['drive_name']}}</h4>
                </div>
                <div class="form-group row">
                    <h3 class="column" style="width: 15%;text-align: right">drive_address :</h3>
                    <h4 class="column" style="margin-top: 4px;margin-left: 3%">{{$patient['drive_address']}}</h4>
                </div>
                <div class="form-group row">
                    <h3 class="column" style="width: 15%;text-align: right">drive_province :</h3>
                    <h4 class="column" style="margin-top: 4px;margin-left: 3%">{{$patient['drive_province']}}</h4>
                </div>
                <div class="form-group row">
                    <h3 class="column" style="width: 15%;text-align: right">tp_no :</h3>
                    <h4 class="column" style="margin-top: 4px;margin-left: 3%">{{$patient['tp_no']}}</h4>
                </div>
                <div class="form-group row">
                    <h3 class="column" style="width: 15%;text-align: right">date_rec :</h3>
                    <h4 class="column" style="margin-top: 4px;margin-left: 3%">{{$patient['date_rec']}}</h4>
                </div>
                <div class="form-group row">
                    <h3 class="column" style="width: 15%;text-align: right">time_rec :</h3>
                    <h4 class="column" style="margin-top: 4px;margin-left: 3%">{{$patient['time_rec']}}</h4>
                </div>
                <div class="form-group row">
                    <h3 class="column" style="width: 15%;text-align: right">acc_subdist :</h3>
                    <h4 class="column" style="margin-top: 4px;margin-left: 3%">{{$patient['acc_subdist']}}</h4>
                </div>
                <div class="form-group row">
                    <h3 class="column" style="width: 15%;text-align: right">acc_dist :</h3>
                    <h4 class="column" style="margin-top: 4px;margin-left: 3%">{{$patient['acc_dist']}}</h4>
                </div>
                <div class="form-group row">
                    <h3 class="column" style="width: 15%;text-align: right">acc_prov :</h3>
                    <h4 class="column" style="margin-top: 4px;margin-left: 3%">{{$patient['acc_prov']}}</h4>
                </div>
                <div class="form-group row">
                    <h3 class="column" style="width: 15%;text-align: right">acc_lat :</h3>
                    <h4 class="column" style="margin-top: 4px;margin-left: 3%">{{$patient['acc_lat']}}</h4>
                </div>
                <div class="form-group row">
                    <h3 class="column" style="width: 15%;text-align: right">acc_long :</h3>
                    <h4 class="column" style="margin-top: 4px;margin-left: 3%">{{$patient['acc_long']}}</h4>
                </div>
                <div class="form-group row">
                    <h3 class="column" style="width: 15%;text-align: right">is_death :</h3>
                    <h4 class="column" style="margin-top: 4px;margin-left: 3%">{{$patient['is_death']}}</h4>
                </div>
                <div class="form-group row">
                    <h3 class="column" style="width: 15%;text-align: right">is_e_claim :</h3>
                    <h4 class="column" style="margin-top: 4px;margin-left: 3%">{{$patient['is_e_claim']}}</h4>
                </div>
                <div class="form-group row">
                    <h3 class="column" style="width: 15%;text-align: right">is_polis :</h3>
                    <h4 class="column" style="margin-top: 4px;margin-left: 3%">{{$patient['is_polis']}}</h4>
                </div>
                <div class="form-group row">
                    <h3 class="column" style="width: 15%;text-align: right">protocol :</h3>
                    <h4 class="column" style="margin-top: 4px;margin-left: 3%">{{$patient['protocol']}}</h4>
                </div>
                <div class="form-group row">
                    <h3 class="column" style="width: 15%;text-align: right">remark :</h3>
                    <h4 class="column" style="margin-top: 4px;margin-left: 3%">{{$patient['remark']}}</h4>
                </div>
                <div class="form-group row">
                    <h3 class="column" style="width: 15%;text-align: right">n_cause :</h3>
                    <h4 class="column" style="margin-top: 4px;margin-left: 3%">{{$patient['n_cause']}}</h4>
                </div>
                <div class="form-group row">
                    <h3 class="column" style="width: 15%;text-align: right">group_riskpoint :</h3>
                    <h4 class="column" style="margin-top: 4px;margin-left: 3%">{{$patient['group_riskpoint']}}</h4>
                </div>
                <div class="form-group row">
                    <h3 class="column" style="width: 15%;text-align: right">group_risk :</h3>
                    <h4 class="column" style="margin-top: 4px;margin-left: 3%">{{$patient['group_risk']}}</h4>
                </div>
            </div>
        </form>
    </div>
{{--    <div class="row">--}}
{{--        <div class="col-lg-12 margin-tb">--}}
{{--            <div class="pull-left">--}}
{{--                <h2> Show Product</h2>--}}
{{--            </div>--}}
{{--            <div class="pull-right"> :{{--                <a class="btn btn-primary" href="/patient"> Back</a>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}

{{--    <div class="row">--}}
{{--        <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--            <div class="form-group">--}}
{{--                <strong>dead_conso:</strong>--}}
{{--                {{$patient['dead_conso']}}--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--            <div class="form-group">--}}
{{--                <strong>dead_year:</strong>--}}
{{--                {{$patient['dead_year']}}--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--            <div class="form-group">--}}
{{--                <strong>acc_no:</strong>--}}
{{--                {{$patient['acc_no']}}--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--            <div class="form-group">--}}
{{--                <strong>fname:</strong>--}}
{{--                {{$patient['fname']}}--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--            <div class="form-group">--}}
{{--                <strong>lname:</strong>--}}
{{--                {{$patient['lname']}}--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--            <div class="form-group">--}}
{{--                <strong>prefix:</strong>--}}
{{--                {{$patient['prefix']}}--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--            <div class="form-group">--}}
{{--                <strong>drv_socno:</strong>--}}
{{--                {{$patient['drv_socno']}}--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--            <div class="form-group">--}}
{{--                <strong>age:</strong>--}}
{{--                {{$patient['age']}}--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--            <div class="form-group">--}}
{{--                <strong>sex:</strong>--}}
{{--                {{$patient['sex']}}--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--            <div class="form-group">--}}
{{--                <strong>birth_date:</strong>--}}
{{--                {{$patient['birth_date']}}--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--            <div class="form-group">--}}
{{--                <strong>career_id:</strong>--}}
{{--                {{$patient['career_id']}}--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--            <div class="form-group">--}}
{{--                <strong>nationality:</strong>--}}
{{--                {{$patient['nationality']}}--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--            <div class="form-group">--}}
{{--                <strong>subdistrict:</strong>--}}
{{--                {{$patient['subdistrict']}}--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--            <div class="form-group">--}}
{{--                <strong>district:</strong>--}}
{{--                {{$patient['district']}}--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--            <div class="form-group">--}}
{{--                <strong>province:</strong>--}}
{{--                {{$patient['province']}}--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--            <div class="form-group">--}}
{{--                <strong>risk_algohol:</strong>--}}
{{--                {{$patient['risk_algohol']}}--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--            <div class="form-group">--}}
{{--                <strong>risk_helmet:</strong>--}}
{{--                {{$patient['risk_helmet']}}--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--            <div class="form-group">--}}
{{--                <strong>risk_algohol:</strong>--}}
{{--                {{$patient['risk_algohol']}}--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--            <div class="form-group">--}}
{{--                <strong>risk_safety:</strong>--}}
{{--                {{$patient['risk_safety']}}--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--            <div class="form-group">--}}
{{--                <strong>dead_date:</strong>--}}
{{--                {{$patient['dead_date']}}--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--            <div class="form-group">--}}
{{--                <strong>victim_no:</strong>--}}
{{--                {{$patient['victim_no']}}--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--            <div class="form-group">--}}
{{--                <strong>car_license:</strong>--}}
{{--                {{$patient['car_license']}}--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--            <div class="form-group">--}}
{{--                <strong>car_province:</strong>--}}
{{--                {{$patient['car_province']}}--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--            <div class="form-group">--}}
{{--                <strong>type_motor:</strong>--}}
{{--                {{$patient['type_motor']}}--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--            <div class="form-group">--}}
{{--                <strong>car_band:</strong>--}}
{{--                {{$patient['car_band']}}--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--            <div class="form-group">--}}
{{--                <strong>drive_name:</strong>--}}
{{--                {{$patient['drive_name']}}--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--            <div class="form-group">--}}
{{--                <strong>drive_address:</strong>--}}
{{--                {{$patient['drive_address']}}--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--            <div class="form-group">--}}
{{--                <strong>drive_province:</strong>--}}
{{--                {{$patient['drive_province']}}--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--            <div class="form-group">--}}
{{--                <strong>tp_no:</strong>--}}
{{--                {{$patient['tp_no']}}--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--            <div class="form-group">--}}
{{--                <strong>date_rec:</strong>--}}
{{--                {{$patient['date_rec']}}--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--            <div class="form-group">--}}
{{--                <strong>time_rec:</strong>--}}
{{--                {{$patient['time_rec']}}--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--            <div class="form-group">--}}
{{--                <strong>acc_subdist:</strong>--}}
{{--                {{$patient['acc_subdist']}}--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--            <div class="form-group">--}}
{{--                <strong>acc_dist:</strong>--}}
{{--                {{$patient['acc_dist']}}--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--            <div class="form-group">--}}
{{--                <strong>acc_prov:</strong>--}}
{{--                {{$patient['acc_prov']}}--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--            <div class="form-group">--}}
{{--                <strong>acc_lat:</strong>--}}
{{--                {{$patient['acc_lat']}}--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--            <div class="form-group">--}}
{{--                <strong>acc_long:</strong>--}}
{{--                {{$patient['acc_long']}}--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--            <div class="form-group">--}}
{{--                <strong>is_death:</strong>--}}
{{--                {{$patient['is_death']}}--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--            <div class="form-group">--}}
{{--                <strong>is_e_claim:</strong>--}}
{{--                {{$patient['is_e_claim']}}--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--            <div class="form-group">--}}
{{--                <strong>is_polis:</strong>--}}
{{--                {{$patient['is_polis']}}--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--            <div class="form-group">--}}
{{--                <strong>protocol:</strong>--}}
{{--                {{$patient['protocol']}}--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--            <div class="form-group">--}}
{{--                <strong>remark:</strong>--}}
{{--                {{$patient['remark']}}--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--            <div class="form-group">--}}
{{--                <strong>n_cause:</strong>--}}
{{--                {{$patient['n_cause']}}--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--            <div class="form-group">--}}
{{--                <strong>group_riskpoint:</strong>--}}
{{--                {{$patient['group_riskpoint']}}--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-xs-12 col-sm-12 col-md-12">--}}
{{--            <div class="form-group">--}}
{{--                <strong>group_risk:</strong>--}}
{{--                {{$patient['group_risk']}}--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
@endsection
