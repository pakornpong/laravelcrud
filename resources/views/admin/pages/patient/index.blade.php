@extends('admin.layouts.main')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div style="text-align: center;font-size: 30px;font-weight: bolder;color: #0b2e13" class="card-header">Patient</div>
                    @if (\Session::has('success'))
                        <div class="alert alert-success">
                            <p>{{ \Session::get('success') }}</p>
                        </div>
                    @endif
                    <div class="card-body">
                        <a href="/patient/create" class="btn btn-primary">Add New</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>dead_conso</th>
                    <th>dead_year</th>
                    <th>acc_no</th>
                    <th>fname</th>
                    <th>lname</th>
                    <th>prefix</th>
                    <th>drv_socno</th>
                    <th>age</th>
                    <th>sex</th>
                    <th>birth_date</th>
                    <th>career_id</th>
                    <th>nationality</th>
                    <th>subdistrict</th>
                    <th>district</th>
                    <th>province</th>
{{--                    <th>risk_algohol</th>--}}
{{--                    <th>risk_helmet</th>--}}
{{--                    <th>risk_safety</th>--}}
                    <th>dead_date</th>
                    <th>victim_no</th>
                    <th>car_license</th>
                    <th>car_province</th>
{{--                    <th>type_motor</th>--}}
                    <th>car_band</th>
                    <th>drive_name</th>
{{--                    <th>drive_address</th>--}}
                    <th>drive_province</th>
                    <th>tp_no</th>
                    <th>date_rec</th>
                    <th>time_rec</th>
                    <th>acc_subdist</th>
                    <th>acc_dist</th>
                    <th>acc_prov</th>
                    <th>acc_lat</th>
                    <th>acc_long</th>
                    <th>is_death</th>
                    <th>is_e_claim</th>
                    <th>is_polis</th>
                    <th>protocol</th>
{{--                    <th>remark</th>--}}
                    <th>n_cause</th>
                    <th>group_riskpoint</th>
                    <th>group_risk</th>
                    <th>View</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
                </thead>
                @foreach($patients as $patient)
                    <tbody>
                    <tr>
                        <td>{{ $patient['id'] }}</td>
                        <td>{{ $patient['dead_conso'] }}</td>
                        <td>{{ $patient['dead_year'] }}</td>
                        <td>{{ $patient['acc_no'] }}</td>
                        <td>{{ $patient['fname'] }}</td>
                        <td>{{ $patient['lname'] }}</td>
                        <td>{{ $patient['prefix'] }}</td>
                        <td>{{ $patient['drv_socno'] }}</td>
                        <td>{{ $patient['age'] }}</td>
                        <td>{{ $patient['sex'] }}</td>
                        <td>{{ $patient['birth_date'] }}</td>
                        <td>{{ $patient['career_id'] }}</td>
                        <td>{{ $patient['nationality'] }}</td>
                        <td>{{ $patient['subdistrict'] }}</td>
                        <td>{{ $patient['district'] }}</td>
                        <td>{{ $patient['province'] }}</td>
{{--                        <td>{{ $patient['risk_algohol'] }}</td>--}}
{{--                        <td>{{ $patient['risk_helmet'] }}</td>--}}
{{--                        <td>{{ $patient['risk_safety'] }}</td>--}}
                        <td>{{ $patient['dead_date'] }}</td>
                        <td>{{ $patient['victim_no'] }}</td>
                        <td>{{ $patient['car_license'] }}</td>
                        <td>{{ $patient['car_province'] }}</td>
{{--                        <td>{{ $patient['type_motor'] }}</td>--}}
                        <td>{{ $patient['car_band'] }}</td>
                        <td>{{ $patient['drive_name'] }}</td>
{{--                        <td>{{ $patient['drive_address'] }}</td>--}}
                        <td>{{ $patient['drive_province'] }}</td>
                        <td>{{ $patient['tp_no'] }}</td>
                        <td>{{ $patient['date_rec'] }}</td>
                        <td>{{ $patient['time_rec'] }}</td>
                        <td>{{ $patient['acc_subdist'] }}</td>
                        <td>{{ $patient['acc_dist'] }}</td>
                        <td>{{ $patient['acc_prov'] }}</td>
                        <td>{{ $patient['acc_lat'] }}</td>
                        <td>{{ $patient['acc_long'] }}</td>
                        <td>{{ $patient['is_death'] }}</td>
                        <td>{{ $patient['is_e_claim'] }}</td>
                        <td>{{ $patient['is_polis'] }}</td>
                        <td>{{ $patient['protocol'] }}</td>
{{--                        <td>{{ $patient['remark'] }}</td>--}}
                        <td>{{ $patient['n_cause'] }}</td>
                        <td>{{ $patient['group_riskpoint'] }}</td>
                        <td>{{ $patient['group_risk'] }}</td>
                        <td><a href="{{action('PatientController@show',$patient['id'])}}" class="btn btn-primary">View</a></td>
                        <td><a href="{{action('PatientController@edit',$patient['id'])}}" class="btn btn-primary">Edit</a></td>

                        <td>
                            <form method="post" class="delete_form" action="{{action('PatientController@destroy',$patient['id'])}}">
                                {{csrf_field()}}
                                <input type="hidden" name="_method" value="DELETE"/>
                                <button type="submit" class="btn btn-danger"  onclick="return confirm('คุณต้องการลบข้อมูลหรือไม่ ?')">Delete</button>
                            </form>
                        </td>
                    </tr>
                    </tbody>
                @endforeach
            </table>
        </div>
    </div>
    <style>
        body {
            overflow:auto;
        }
    </style>
    {{--    <script type="text/javascript">--}}
    {{--        $(document).ready(function(){--}}
    {{--            $('.delete_form').on('submit',function () {--}}
    {{--                if (confirm("คุณต้องการลบข้อมูลหรือไม่ ?")){--}}
    {{--                    return true;--}}
    {{--                }--}}
    {{--                else {--}}
    {{--                    return false;--}}
    {{--                }--}}
    {{--            });--}}
    {{--        });--}}
    {{--    </script>--}}
@endsection
