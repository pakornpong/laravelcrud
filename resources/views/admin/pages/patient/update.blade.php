@extends('admin.layouts.main')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div  style="text-align: center;font-size: 30px;font-weight: bolder;color: #0b2e13"  class="card-header">Edit Patient</div>
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul> @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    @if (\Session::has('success'))
                        <div class="alert alert-success">
                            <p>{{ \Session::get('success') }}</p>
                        </div>
                    @endif
                    <div class="card-body">
                        <a href="/patient" class="btn btn-primary">ย้อนกลับ</a>
                    </div>
                </div>
            </div>
        </div>
        <form method="POST" action="{{ route('patient.update',['patient'=>$id]) }}">
            {{csrf_field()}}
            <input name="_method" type="hidden" value="PUT">

            <div class="form-group row">
                 <h3 class="column" style="width: 15%;text-align: right">dead_conso: </h3>
                <input class="column" style="margin-left: 1%;padding-left: 1%;padding-bottom: 4px" type="text" name="dead_conso" class="form-control" placeholder="dead_conso" value="{{$patient['dead_conso']}}" />
            </div>
            <div class="form-group row">
                 <h3 class="column" style="width: 15%;text-align: right">dead_year: </h3>
                <input class="column" style="margin-left: 1%;padding-left: 1%;padding-bottom: 4px" type="text" name="dead_year" class="form-control" placeholder="dead_year" value="{{$patient['dead_year']}}" />
            </div>
            <div class="form-group row">
                 <h3 class="column" style="width: 15%;text-align: right">acc_no: </h3>
                <input class="column" style="margin-left: 1%;padding-left: 1%;padding-bottom: 4px" type="text" name="acc_no" class="form-control" placeholder="acc_no" value="{{$patient['acc_no']}}" />
            </div>
            <div class="form-group row">
                 <h3 class="column" style="width: 15%;text-align: right">fname: </h3>
                <input class="column" style="margin-left: 1%;padding-left: 1%;padding-bottom: 4px" type="text" name="fname" class="form-control" placeholder="fname" value="{{$patient['fname']}}" />
            </div>
            <div class="form-group row">
                 <h3 class="column" style="width: 15%;text-align: right">lname: </h3>
                <input class="column" style="margin-left: 1%;padding-left: 1%;padding-bottom: 4px" type="text" name="lname" class="form-control" placeholder="lname" value="{{$patient['lname']}}" />
            </div>
            <div class="form-group row">
                 <h3 class="column" style="width: 15%;text-align: right">prefix: </h3>
                <input class="column" style="margin-left: 1%;padding-left: 1%;padding-bottom: 4px" type="text" name="prefix" class="form-control" placeholder="prefix" value="{{$patient['prefix']}}" />
            </div>
            <div class="form-group row">
                 <h3 class="column" style="width: 15%;text-align: right">drv_socno: </h3>
                <input class="column" style="margin-left: 1%;padding-left: 1%;padding-bottom: 4px" type="text" name="drv_socno" class="form-control" placeholder="drv_socno" value="{{$patient['drv_socno']}}" />
            </div>
            <div class="form-group row">
                 <h3 class="column" style="width: 15%;text-align: right">age: </h3>
                <input class="column" style="margin-left: 1%;padding-left: 1%;padding-bottom: 4px" type="text" name="age" class="form-control" placeholder="age" value="{{$patient['age']}}" />
            </div>
            <div class="form-group row">
                 <h3 class="column" style="width: 15%;text-align: right">sex: </h3>
                <input class="column" style="margin-left: 1%;padding-left: 1%;padding-bottom: 4px" type="text" name="sex" class="form-control" placeholder="sex" value="{{$patient['sex']}}" />
            </div>
            <div class="form-group row">
                 <h3 class="column" style="width: 15%;text-align: right">birth_date: </h3>
                <input class="column" style="margin-left: 1%;padding-left: 1%;padding-bottom: 4px" type="text" name="birth_date" class="form-control" placeholder="birth_date" value="{{$patient['birth_date']}}" />
            </div>
            <div class="form-group row">
                 <h3 class="column" style="width: 15%;text-align: right">career_id: </h3>
                <input class="column" style="margin-left: 1%;padding-left: 1%;padding-bottom: 4px" type="text" name="career_id" class="form-control" placeholder="career_id" value="{{$patient['career_id']}}" />
            </div>
            <div class="form-group row">
                 <h3 class="column" style="width: 15%;text-align: right">nationality: </h3>
                <input class="column" style="margin-left: 1%;padding-left: 1%;padding-bottom: 4px" type="text" name="nationality" class="form-control" placeholder="nationality" value="{{$patient['nationality']}}" />
            </div>
            <div class="form-group row">
                 <h3 class="column" style="width: 15%;text-align: right">subdistrict: </h3>
                <input class="column" style="margin-left: 1%;padding-left: 1%;padding-bottom: 4px" type="text" name="subdistrict" class="form-control" placeholder="subdistrict" value="{{$patient['subdistrict']}}" />
            </div>
            <div class="form-group row">
                 <h3 class="column" style="width: 15%;text-align: right">district: </h3>
                <input class="column" style="margin-left: 1%;padding-left: 1%;padding-bottom: 4px" type="text" name="district" class="form-control" placeholder="district" value="{{$patient['district']}}" />
            </div>
            <div class="form-group row">
                 <h3 class="column" style="width: 15%;text-align: right">province: </h3>
                <input class="column" style="margin-left: 1%;padding-left: 1%;padding-bottom: 4px" type="text" name="province" class="form-control" placeholder="province" value="{{$patient['province']}}" />
            </div>
{{--            <div class="form-group row">
{{----}}{{-- <h3 class="column" style="width: 15%;text-align: right">risk_algohol: </h3>--}}
{{--                <input class="column" style="margin-left: 1%;padding-left: 1%;padding-bottom: 4px" type="text" name="risk_algohol" class="form-control" placeholder="risk_algohol" value="{{$patient['risk_algohol']}}" />--}}
{{--            </div>--}}
{{--            <div class="form-group row">
{{----}}{{-- <h3 class="column" style="width: 15%;text-align: right">risk_helmet: </h3>--}}
{{--                <input class="column" style="margin-left: 1%;padding-left: 1%;padding-bottom: 4px" type="text" name="risk_helmet" class="form-control" placeholder="risk_helmet" value="{{$patient['risk_helmet']}}" />--}}
{{--            </div>--}}
{{--            <div class="form-group row">
{{----}}{{-- <h3 class="column" style="width: 15%;text-align: right">risk_safety: </h3>--}}
{{--                <input class="column" style="margin-left: 1%;padding-left: 1%;padding-bottom: 4px" type="text" name="risk_safety" class="form-control" placeholder="risk_safety" value="{{$patient['risk_safety']}}" />--}}
{{--            </div>--}}
            <div class="form-group row">
                 <h3 class="column" style="width: 15%;text-align: right">dead_date: </h3>
                <input class="column" style="margin-left: 1%;padding-left: 1%;padding-bottom: 4px" type="text" name="dead_date" class="form-control" placeholder="dead_date" value="{{$patient['dead_date']}}" />
            </div>
            <div class="form-group row">
                 <h3 class="column" style="width: 15%;text-align: right">victim_no: </h3>
                <input class="column" style="margin-left: 1%;padding-left: 1%;padding-bottom: 4px" type="text" name="victim_no" class="form-control" placeholder="victim_no" value="{{$patient['victim_no']}}" />
            </div>
            <div class="form-group row">
                 <h3 class="column" style="width: 15%;text-align: right">car_license: </h3>
                <input class="column" style="margin-left: 1%;padding-left: 1%;padding-bottom: 4px" type="text" name="car_license" class="form-control" placeholder="car_license" value="{{$patient['car_license']}}" />
            </div>
            <div class="form-group row">
                 <h3 class="column" style="width: 15%;text-align: right">car_province: </h3>
                <input class="column" style="margin-left: 1%;padding-left: 1%;padding-bottom: 4px" type="text" name="car_province" class="form-control" placeholder="car_province" value="{{$patient['car_province']}}" />
            </div>
{{--            <div class="form-group row">
{{----}}{{-- <h3 class="column" style="width: 15%;text-align: right">type_motor: </h3>--}}
{{--                <input class="column" style="margin-left: 1%;padding-left: 1%;padding-bottom: 4px" type="text" name="type_motor" class="form-control" placeholder="type_motor" value="{{$patient['type_motor']}}" />--}}
{{--            </div>--}}
            <div class="form-group row">
                 <h3 class="column" style="width: 15%;text-align: right">car_band: </h3>
                <input class="column" style="margin-left: 1%;padding-left: 1%;padding-bottom: 4px" type="text" name="car_band" class="form-control" placeholder="car_band" value="{{$patient['car_band']}}" />
            </div>
            <div class="form-group row">
                 <h3 class="column" style="width: 15%;text-align: right">drive_name: </h3>
                <input class="column" style="margin-left: 1%;padding-left: 1%;padding-bottom: 4px" type="text" name="drive_name" class="form-control" placeholder="drive_name" value="{{$patient['drive_name']}}" />
            </div>
{{--            <div class="form-group row">
{{----}}{{-- <h3 class="column" style="width: 15%;text-align: right">drive_address: </h3>--}}
{{--                <input class="column" style="margin-left: 1%;padding-left: 1%;padding-bottom: 4px" type="text" name="drive_address" class="form-control" placeholder="drive_address" value="{{$patient['drive_address']}}" />--}}
{{--            </div>--}}
            <div class="form-group row">
                 <h3 class="column" style="width: 15%;text-align: right">drive_province: </h3>
                <input class="column" style="margin-left: 1%;padding-left: 1%;padding-bottom: 4px" type="text" name="drive_province" class="form-control" placeholder="drive_province" value="{{$patient['drive_province']}}" />
            </div>
            <div class="form-group row">
                 <h3 class="column" style="width: 15%;text-align: right">tp_no: </h3>
                <input class="column" style="margin-left: 1%;padding-left: 1%;padding-bottom: 4px" type="text" name="tp_no" class="form-control" placeholder="tp_no" value="{{$patient['tp_no']}}" />
            </div>
            <div class="form-group row">
                 <h3 class="column" style="width: 15%;text-align: right">date_rec: </h3>
                <input class="column" style="margin-left: 1%;padding-left: 1%;padding-bottom: 4px" type="text" name="date_rec" class="form-control" placeholder="date_rec" value="{{$patient['date_rec']}}" />
            </div>
            <div class="form-group row">
                 <h3 class="column" style="width: 15%;text-align: right">time_rec: </h3>
                <input class="column" style="margin-left: 1%;padding-left: 1%;padding-bottom: 4px" type="text" name="time_rec" class="form-control" placeholder="time_rec" value="{{$patient['time_rec']}}" />
            </div>
            <div class="form-group row">
                 <h3 class="column" style="width: 15%;text-align: right">acc_subdist: </h3>
                <input class="column" style="margin-left: 1%;padding-left: 1%;padding-bottom: 4px" type="text" name="acc_subdist" class="form-control" placeholder="acc_subdist" value="{{$patient['acc_subdist']}}" />
            </div>
            <div class="form-group row">
                 <h3 class="column" style="width: 15%;text-align: right">acc_dist: </h3>
                <input class="column" style="margin-left: 1%;padding-left: 1%;padding-bottom: 4px" type="text" name="acc_dist" class="form-control" placeholder="acc_dist" value="{{$patient['acc_dist']}}" />
            </div>
            <div class="form-group row">
                 <h3 class="column" style="width: 15%;text-align: right">acc_prov: </h3>
                <input class="column" style="margin-left: 1%;padding-left: 1%;padding-bottom: 4px" type="text" name="acc_prov" class="form-control" placeholder="acc_prov" value="{{$patient['acc_prov']}}" />
            </div>
            <div class="form-group row">
                 <h3 class="column" style="width: 15%;text-align: right">acc_dist: </h3>
                <input class="column" style="margin-left: 1%;padding-left: 1%;padding-bottom: 4px" type="text" name="acc_dist" class="form-control" placeholder="acc_dist" value="{{$patient['acc_dist']}}" />
            </div>
            <div class="form-group row">
                 <h3 class="column" style="width: 15%;text-align: right">acc_lat: </h3>
                <input class="column" style="margin-left: 1%;padding-left: 1%;padding-bottom: 4px" type="text" name="acc_lat" class="form-control" placeholder="acc_lat" value="{{$patient['acc_lat']}}" />
            </div>
            <div class="form-group row">
                 <h3 class="column" style="width: 15%;text-align: right">acc_long: </h3>
                <input class="column" style="margin-left: 1%;padding-left: 1%;padding-bottom: 4px" type="text" name="acc_long" class="form-control" placeholder="acc_long" value="{{$patient['acc_long']}}" />
            </div>
            <div class="form-group row">
                 <h3 class="column" style="width: 15%;text-align: right">is_death: </h3>
                <input class="column" style="margin-left: 1%;padding-left: 1%;padding-bottom: 4px" type="text" name="is_death" class="form-control" placeholder="is_death" value="{{$patient['is_death']}}" />
            </div>
            <div class="form-group row">
                 <h3 class="column" style="width: 15%;text-align: right">is_e_claim: </h3>
                <input class="column" style="margin-left: 1%;padding-left: 1%;padding-bottom: 4px" type="text" name="is_e_claim" class="form-control" placeholder="is_e_claim" value="{{$patient['is_e_claim']}}" />
            </div>
            <div class="form-group row">
                 <h3 class="column" style="width: 15%;text-align: right">is_polis: </h3>
                <input class="column" style="margin-left: 1%;padding-left: 1%;padding-bottom: 4px" type="text" name="is_polis" class="form-control" placeholder="is_polis" value="{{$patient['is_polis']}}" />
            </div>
            <div class="form-group row">
                 <h3 class="column" style="width: 15%;text-align: right">protocol: </h3>
                <input class="column" style="margin-left: 1%;padding-left: 1%;padding-bottom: 4px" type="text" name="protocol" class="form-control" placeholder="protocol" value="{{$patient['protocol']}}" />
            </div>
            <div class="form-group row">
                 <h3 class="column" style="width: 15%;text-align: right">remark: </h3>
                <input class="column" style="margin-left: 1%;padding-left: 1%;padding-bottom: 4px" type="text" name="remark" class="form-control" placeholder="remark" value="{{$patient['remark']}}" />
            </div>
            <div class="form-group row">
                 <h3 class="column" style="width: 15%;text-align: right">n_cause: </h3>
                <input class="column" style="margin-left: 1%;padding-left: 1%;padding-bottom: 4px" type="text" name="n_cause" class="form-control" placeholder="n_cause" value="{{$patient['n_cause']}}" />
            </div>
            <div class="form-group row">
                 <h3 class="column" style="width: 15%;text-align: right">group_riskpoint: </h3>
                <input class="column" style="margin-left: 1%;padding-left: 1%;padding-bottom: 4px" type="text" name="group_riskpoint" class="form-control" placeholder="group_riskpoint" value="{{$patient['group_riskpoint']}}" />
            </div>
            <div class="form-group row">
                 <h3 class="column" style="width: 15%;text-align: right">group_risk: </h3>
                <input class="column" style="margin-left: 1%;padding-left: 1%;padding-bottom: 4px" type="text" name="group_risk" class="form-control" placeholder="group_risk" value="{{$patient['group_risk']}}" />
            </div>

            <div class="form-group " style="text-align: center">

                {{--                <input type="hidden" name="_method" value="PATCH"/>--}}
                <input  type="submit" class="btn btn-primary" value="อัพเดต">
            </div>

        </form>
    </div>
@endsection
