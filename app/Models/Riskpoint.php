<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Riskpoint extends Model
{
    protected $table = 'riskpoint';
    protected $primaryKey = 'id';
    protected $fillable = ['group_riskpoint','name_riskpoint'];
}
