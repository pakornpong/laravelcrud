<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    protected $table = 'dead_info';
    protected $primaryKey = 'id';
    protected $fillable = [
        'dead_conso',
        'dead_year',
        'acc_no',
        'fname',
        'lname',
        'prefix',
        'drv_socno',
        'age',
        'sex',
        'birth_date',
        'career_id',
        'nationality',
        'subdistrict',
        'district',
        'province',
        'risk_algohol',
        'risk_helmet',
        'risk_safety',
        'dead_date',
        'victim_no',
        'car_license',
        'car_province',
        'type_motor',
        'car_band',
        'drive_name',
        'drive_address',
        'drive_province',
        'tp_no',
        'date_rec',
        'time_rec',
        'acc_subdist',
        'acc_dist',
        'acc_prov',
        'acc_lat',
        'acc_long',
        'is_death',
        'is_e_claim',
        'is_polis',
        'protocol',
        'remark',
        'n_cause',
        'group_riskpoint',
        'group_risk'];
//allfilable41
}
