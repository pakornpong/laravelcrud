<?php

namespace App\Http\Controllers;
use App\Models\Patient;
use Illuminate\Http\Request;

class PatientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $patients = Patient::all()->toArray();
        return view('admin.pages.patient.index',compact('patients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.patient.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'dead_conso' => 'required',
            'dead_year' => 'required',
            'acc_no' => 'required',
            'fname' => 'required',
            'lname' => 'required',
            'prefix' => 'required',
            'drv_socno' => 'required',
            'age' => 'required',
            'sex' => 'required',
            'birth_date' => 'required',
            'career_id' => 'required',
            'nationality' => 'required',
            'subdistrict' => 'required',
            'district' => 'required',
            'province' => 'required',
            //'risk_algohol' => 'required',
            //'risk_helmet' => 'required',
            //'risk_safety' => 'required',
            'dead_date' => 'required',
            'victim_no' => 'required',
            'car_license' => 'required',
            'car_province' => 'required',
//            'type_motor' => 'required',
            'car_band' => 'required',
            'drive_name' => 'required',
//            'drive_address' => 'required',
            'drive_province' => 'required',
            'tp_no' => 'required',
            'date_rec' => 'required',
            'time_rec' => 'required',
            'acc_subdist' => 'required',
            'acc_dist' => 'required',
            'acc_prov' => 'required',
            'acc_lat' => 'required',
            'acc_long' => 'required',
            'is_death' => 'required',
            'is_e_claim' => 'required',
            'is_polis' => 'required',
            'protocol' => 'required',
            'n_cause' => 'required',
            'group_riskpoint' => 'required',
            'group_risk' => 'required'
        ]);
        $patient = new Patient([
//            'dead_conso' => $request->get('dead_conso'),
//            'dead_year' => $request->get('dead_year')
            'dead_conso' => $request->get('dead_conso'),
            'dead_year' => $request->get('dead_year'),
            'acc_no' => $request->get('acc_no'),
            'fname' => $request->get('fname'),
            'lname' => $request->get('lname'),
            'prefix' => $request->get('prefix'),
            'drv_socno' => $request->get('drv_socno'),
            'age' => $request->get('age'),
            'sex' => $request->get('sex'),
            'birth_date' => $request->get('birth_date'),
            'career_id' => $request->get('career_id'),
            'nationality' => $request->get('nationality'),
            'subdistrict' => $request->get('subdistrict'),
            'district' => $request->get('district'),
            'province' => $request->get('province'),
            //'risk_algohol' => $request->get('risk_algohol'),
            //'risk_helmet' => $request->get('risk_helmet'),
            //'risk_safety' => $request->get('risk_safety'),
            'dead_date' => $request->get('dead_date'),
            'victim_no' => $request->get('victim_no'),
            'car_license' => $request->get('car_license'),
            'car_province' => $request->get('car_province'),
//            'type_motor' => $request->get('type_motor'),
            'car_band' => $request->get('car_band'),
            'drive_name' => $request->get('drive_name'),
//            'drive_address' => $request->get('drive_address'),
            'drive_province' => $request->get('drive_province'),
            'tp_no' => $request->get('tp_no'),
            'date_rec' => $request->get('date_rec'),
            'time_rec' => $request->get('time_rec'),
            'acc_subdist' => $request->get('acc_subdist'),
            'acc_dist' => $request->get('acc_dist'),
            'acc_prov' => $request->get('acc_prov'),
            'acc_lat' => $request->get('acc_lat'),
            'acc_long' => $request->get('acc_long'),
            'is_death' => $request->get('is_death'),
            'is_e_claim' => $request->get('is_e_claim'),
            'is_polis' => $request->get('is_polis'),
            'protocol' => $request->get('protocol'),
            'n_cause' => $request->get('n_cause'),
            'group_riskpoint' => $request->get('group_riskpoint'),
            'group_risk' => $request->get('group_risk')
        ]);
        $patient->save();
        return redirect()->route('patient.create')->with('success','บันทึกข้อมูลสำเร็จ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $patient = Patient::find($id);
        return view('admin.pages.patient.show',compact('patient','id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $patient = Patient::find($id);
        $response = array();
        $response['id'] = $id;
        $response['patient'] = $patient;
        return view('admin.pages.patient.update')->with($response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'dead_conso' => 'required',
            'dead_year' => 'required',
            'acc_no' => 'required',
            'fname' => 'required',
            'lname' => 'required',
            'prefix' => 'required',
            'drv_socno' => 'required',
            'age' => 'required',
            'sex' => 'required',
            'birth_date' => 'required',
            'career_id' => 'required',
            'nationality' => 'required',
            'subdistrict' => 'required',
            'district' => 'required',
            'province' => 'required',
            //'risk_algohol' => 'required',
            //'risk_helmet' => 'required',
            //'risk_safety' => 'required',
            'dead_date' => 'required',
            'victim_no' => 'required',
            'car_license' => 'required',
            'car_province' => 'required',
//            'type_motor' => 'required',
            'car_band' => 'required',
            'drive_name' => 'required',
//            'drive_address' => 'required',
            'drive_province' => 'required',
            'tp_no' => 'required',
            'date_rec' => 'required',
            'time_rec' => 'required',
            'acc_subdist' => 'required',
            'acc_dist' => 'required',
            'acc_prov' => 'required',
            'acc_lat' => 'required',
            'acc_long' => 'required',
            'is_death' => 'required',
            'is_e_claim' => 'required',
            'is_polis' => 'required',
            'protocol' => 'required',
            'n_cause' => 'required',
            'group_riskpoint' => 'required',
            'group_risk' => 'required'
        ]);
//        $id->update($request->all());
//        return redirect()->route('admin.pages.riskpoint.index')
//            ->with('success','riskpoint updated successfully');
        $patient = Patient::find($id);
        $FieldsUpdate = array();

        $FieldsUpdate['group_riskpoint'] = trim($request->input('group_riskpoint'));
        $FieldsUpdate['name_riskpoint'] = trim($request->input('name_riskpoint'));
        $FieldsUpdate['dead_conso'] = trim($request)->input('dead_conso');
        $FieldsUpdate['dead_year'] = trim($request)->input('dead_year');
        $FieldsUpdate['acc_no'] = trim($request)->input('acc_no');
        $FieldsUpdate['fname'] = trim($request)->input('fname');
        $FieldsUpdate['lname'] = trim($request)->input('lname');
        $FieldsUpdate['prefix'] = trim($request)->input('prefix');
        $FieldsUpdate['drv_socno'] = trim($request)->input('drv_socno');
        $FieldsUpdate['age'] = trim($request)->input('age');
        $FieldsUpdate['sex'] = trim($request)->input('sex');
        $FieldsUpdate['birth_date'] = trim($request)->input('birth_date');
        $FieldsUpdate['career_id'] = trim($request)->input('career_id');
        $FieldsUpdate['nationality'] = trim($request)->input('nationality');
        $FieldsUpdate['subdistrict'] = trim($request)->input('subdistrict');
        $FieldsUpdate['district'] = trim($request)->input('district');
        $FieldsUpdate['province'] = trim($request)->input('province');
//        $FieldsUpdate['risk_algohol'] = trim($request)->input('risk_algohol');
//        $FieldsUpdate['risk_helmet'] = trim($request)->input('risk_helmet');
//        $FieldsUpdate['risk_safety'] = trim($request)->input('risk_safety');
        $FieldsUpdate['dead_date'] = trim($request)->input('dead_date');
        $FieldsUpdate['victim_no'] = trim($request)->input('victim_no');
        $FieldsUpdate['car_license'] = trim($request)->input('car_license');
        $FieldsUpdate['car_province'] = trim($request)->input('car_province');
//        $FieldsUpdate['type_motor'] = trim($request)->input('type_motor');
        $FieldsUpdate['car_band'] = trim($request)->input('car_band');
        $FieldsUpdate['drive_name'] = trim($request)->input('drive_name');
//        $FieldsUpdate['drive_address'] = trim($request)->input('drive_address');
        $FieldsUpdate['drive_province'] = trim($request)->input('drive_province');
        $FieldsUpdate['tp_no'] = trim($request)->input('tp_no');
        $FieldsUpdate['date_rec'] = trim($request)->input('date_rec');
        $FieldsUpdate['time_rec'] = trim($request)->input('time_rec');
        $FieldsUpdate['acc_subdist'] = trim($request)->input('acc_subdist');
        $FieldsUpdate['acc_dist'] = trim($request)->input('acc_dist');
        $FieldsUpdate['acc_prov'] = trim($request)->input('acc_prov');
        $FieldsUpdate['acc_lat'] = trim($request)->input('acc_lat');
        $FieldsUpdate['acc_long'] = trim($request)->input('acc_long');
        $FieldsUpdate['is_death'] = trim($request)->input('is_death');
        $FieldsUpdate['is_e_claim'] = trim($request)->input('is_e_claim');
        $FieldsUpdate['is_polis'] = trim($request)->input('is_polis');
        $FieldsUpdate['protocol'] = trim($request)->input('protocol');
        $FieldsUpdate['remark'] = trim($request)->input('remark');
        $FieldsUpdate['n_cause'] = trim($request)->input('n_cause');
        $FieldsUpdate['group_riskpoint'] = trim($request)->input('group_riskpoint');
        $FieldsUpdate['group_risk'] = trim($request)->input('group_risk');


        $patient->update($FieldsUpdate);
        return redirect()->route('patient.index')->with('success','อัพเดตเรียบร้อย');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $patient = Patient::find($id);
        $patient->delete();
        return redirect()->route('patient.index')->with('success','ลบข้อมูลเรียบร้อย');
    }
}

