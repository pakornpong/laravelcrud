<?php

namespace App\Http\Controllers;
use App\Models\Riskpoint;
use Illuminate\Http\Request;

class RiskpointController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $riskpoints = Riskpoint::all()->toArray();
        return view('admin.pages.riskpoint.index',compact('riskpoints'));
//        $riskpoints = [];
//        $riskpoints ['datatables'] = "riskpoint";
//        return view('admin.pages.riskpoint.index')->with($riskpoints);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.riskpoint.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'group_riskpoint' => 'required',
            'name_riskpoint' => 'required',
        ]);
        $riskpoint = new Riskpoint([
            'group_riskpoint' => $request->get('group_riskpoint'),
            'name_riskpoint' => $request->get('name_riskpoint')
        ]);
        $riskpoint->save();
        return redirect()->route('riskpoint.create')->with('success','บันทึกข้อมูลสำเร็จ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $riskpoint = Riskpoint::find($id);
        return view('admin.pages.riskpoint.show',compact('riskpoint','id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
//        return view('admin.pages.riskpoint.edit',compact('id'));
        $riskpoint = Riskpoint::find($id);
        $response = array();
        $response['id'] = $id;
        $response['riskpoint'] = $riskpoint;
        return view('admin.pages.riskpoint.update')->with($response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'group_riskpoint' => 'required',
            'name_riskpoint' => 'required',
        ]);
//        $id->update($request->all());
//        return redirect()->route('admin.pages.riskpoint.index')
//            ->with('success','riskpoint updated successfully');
        $riskpoint = Riskpoint::find($id);
        $FieldsUpdate = array();
        $FieldsUpdate['group_riskpoint'] = trim($request->input('group_riskpoint'));
        $FieldsUpdate['name_riskpoint'] = trim($request->input('name_riskpoint'));
        $riskpoint->update($FieldsUpdate);
        return redirect()->route('riskpoint.index')->with('success','อัพเดตเรียบร้อย');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $riskpoint = Riskpoint::find($id);
        $riskpoint->delete();
        return redirect()->route('riskpoint.index')->with('success','ลบข้อมูลเรียบร้อย');
//        $id->delete();
//        return redirect()->route('riskpoint.index')
//            ->with('success','riskpoint deleted successfully');
    }
}
